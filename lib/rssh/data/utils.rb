class Module
  def create_method(name, &block)
    self.send(:define_method, name, &block)
  end

  def dup_method(new_name, old_name)
    self.send(:alias_method, new_name, old_name)
  end
end

module Rssh::Utils
  module Singleton
    def instance
      @instance ||= self.new
    end
  end

  module DynamicAccessor
    def getter(name, getter)
      self.create_method(name.to_sym, &getter)
      end
    def setter(name, setter)
      self.create_method("#{name}=".to_sym, &setter)
    end
    def accessor(name, getter, setter)
      self.getter(name, getter)
      self.setter(name, setter)
    end
  end
end