require 'yaml'
require 'everyday_natsort_kernel'
require_relative 'utils'

module Rssh::Data
  class Servers
    extend Rssh::Utils::Singleton
    extend Rssh::Utils::DynamicAccessor

    attr_accessor :list

    accessor :sorted_list, -> { Hash[self.list.natural_sort] }, ->(list) { self.list = Hash[list.natural_sort] }
    getter :file_name, -> { File.expand_path('~/servers.rssh.yaml') }

    def initialize
      self.load_list
    end

    def load_list
      self.list = File.exist?(self.file_name) ? YAML::load_file(self.file_name) : {}
    end

    def save_list
      IO.write(self.file_name, self.list.to_yaml) unless @no_save
    end

    def [](name)
      self.list[name]
    end

    def []=(name, server)
    end

    private :file_name
  end
end