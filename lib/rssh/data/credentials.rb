require 'keychain'
require_relative 'utils'

module Rssh::Data
  class Credentials
    extend Rssh::Utils::Singleton
    extend Rssh::Utils::DynamicAccessor

    getter :keychain_path, -> { File.expand_path('~/Library/Keychains/rssh.keychain') }

    def keychain
      if File.exist?(self.keychain_path)
        Keychain.open(self.keychain_path)
      else
        Keychain.create(self.keychain_path)
      end
    end

    def get(server)
      self.keychain.internet_passwords.where(server: server).first
    end

    def set(server, user, password)
      kc = self.get(server)
      if kc.nil?
        self.keychain.internet_passwords.create(server: server, password: password, account: user)
      else
        kc.account  = user
        kc.password = password
        kc.save!(keychain: self.keychain)
      end
      self.get(server).nil? ? :failure : :success
    end

    def delete(server)
      kc = self.get(server)
      if kc.nil?
        :nil
      else
        kc.delete
        kc = self.get(server)
        kc.nil? ? :success : :failure
      end
    end

    private :keychain_path, :keychain
  end
end