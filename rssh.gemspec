# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'rssh/version'

Gem::Specification.new do |spec|
  spec.name        = 'rssh'
  spec.version     = Rssh::VERSION
  spec.authors     = ['Eric Henderson']
  spec.email       = ['henderea@gmail.com']
  spec.summary     = %q{A ruby utility for managing ssh'ing.}
  spec.description = %q{Allows you to store common servers, credentials, and more to make ssh easier.}
  spec.homepage    = 'https://gitlab.com/henderea/rssh'
  spec.license     = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ['lib']

  spec.required_ruby_version = '>= 1.9.3'

  spec.add_development_dependency 'bundler', '~> 1.7'
  spec.add_development_dependency 'rake', '~> 10.4'

  spec.add_dependency 'everyday_thor_util', '~> 2.0', '>= 2.0.5'
  spec.add_dependency 'thor', '~> 0.19'
  spec.add_dependency 'ruby-keychain', '~> 0.3', '>= 0.3.2'
  spec.add_dependency 'everyday_natsort', '~> 1.0', '>= 1.0.3'
end
